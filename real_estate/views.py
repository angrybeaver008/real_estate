from django.shortcuts import render
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponseRedirect
from django.views.generic import DetailView, View

from .models import NewBuilding, Flats, Cottage, Category, LatestHata, Client, Favorites, FavoriteRealEstate
from .mixins import CategoryDetailMixin, FavoritesMixin


class BaseView(FavoritesMixin, View):

    def get(self, request, *args, **kwargs):
        categories = Category.objects.get_categories_for_sidebar()
        estates = LatestHata.objects.get_hata_for_main_page('newbuilding', 'flats', 'cottage')
        return render(request, 'base.html', {'categories': categories, 'estates': estates, 'favorites': self.favorites})


# def test_view(request):
#     categories = Category.objects.get_categories_for_sidebar()
#     return render(request, 'base.html', {'categories': categories})


class RealEstateDetailView(CategoryDetailMixin, FavoritesMixin, DetailView):
    CT_MODEL_MODEL_CLASS = {
        'newbuilding': NewBuilding,
        'flats': Flats,
        'cottage': Cottage
    }

    def dispatch(self, request, *args, **kwargs):
        self.model = self.CT_MODEL_MODEL_CLASS[kwargs['ct_model']]
        self.queryset = self.model._base_manager.all()
        return super().dispatch(request, *args, *kwargs)

    # model = Model
    # queryset = Model.objects.all()
    context_object_name = 'estate'
    template_name = 'estate_detail.html'
    slug_url_kwarg = 'slug'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ct_model'] = self.model._meta.model_name
        context['favorites'] = self.favorites
        return context


class CategoryDetailView(CategoryDetailMixin, FavoritesMixin, DetailView):
    model = Category
    queryset = Category.objects.all()
    context_object_name = 'category'
    template_name = 'category_detail.html'
    slug_url_kwarg = 'slug'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['ct_model'] = self.model._meta.model_name
        context['favorites'] = self.favorites
        return context


class AddToFavoritesView(FavoritesMixin, View):

    def get(self, request, *args, **kwargs):
        ct_model, estate_slug = kwargs.get('ct_model'), kwargs.get('slug')
        content_type = ContentType.objects.get(model=ct_model)
        estate = content_type.model_class().objects.get(slug=estate_slug)
        favorite_estate, create = FavoriteRealEstate.objects.get_or_create(
            user=self.favorites.owner, favorites=self.favorites, object_id=estate.id,content_type=content_type
        )
        self.favorites.real_estate.add(favorite_estate)
        return HttpResponseRedirect('/real_estate/favorites/')


class DeliteFromFavoritesView(FavoritesMixin, View):

    def get(self, request, *args, **kwargs):
        ct_model, estate_slug = kwargs.get('ct_model'), kwargs.get('slug')
        content_type = ContentType.objects.get(model=ct_model)
        estate = content_type.model_class().objects.get(slug=estate_slug)
        favorite_estate = FavoriteRealEstate.objects.get(
            user=self.favorites.owner, favorites=self.favorites, object_id=estate.id,content_type=content_type
        )
        self.favorites.real_estate.remove(favorite_estate)
        return HttpResponseRedirect('/real_estate/favorites/')


class FavoritesView(FavoritesMixin, View):

    def get(self, request, *args, **kwargs):
        categories = Category.objects.get_categories_for_sidebar()
        return render(request, 'favorites.html', {'favorites': self.favorites, 'categories': categories})
