from django.urls import path
from .views import BaseView, RealEstateDetailView, CategoryDetailView, FavoritesView, AddToFavoritesView, DeliteFromFavoritesView


urlpatterns = [
    path('', BaseView.as_view(), name='base'),
    path('estate/<str:ct_model>/<str:slug>', RealEstateDetailView.as_view(), name='estate_detail'),
    path('estate/<str:slug>', CategoryDetailView.as_view(), name='category_detail'),
    path('favorites/', FavoritesView.as_view(), name='favorites'),
    path('add_to_favorites/<str:ct_model>/<str:slug>', AddToFavoritesView.as_view(), name='add_to_favorites'),
    path('delete_from_favorites/<str:ct_model>/<str:slug>', DeliteFromFavoritesView.as_view(), name='delete_from_favorites'),
]
