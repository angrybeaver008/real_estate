from django.db import models
from decimal import Decimal
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey
from django.urls import reverse

User = get_user_model()


def get_models_count(*model_names):
    return [models.Count(model_name) for model_name in model_names]


def get_real_estate_url(obj, viewname):
    ct_model = obj.__class__._meta.model_name
    return reverse(viewname, kwargs={'ct_model': ct_model, 'slug': obj.slug})


class LatestHataManeger:
    @staticmethod
    def get_hata_for_main_page(*args, **kwargs):
        with_respect_to = kwargs.get('with_respect_to')
        hata = []
        ct_models = ContentType.objects.filter(model__in=args)
        for ct_model in ct_models:
            model_hata = ct_model.model_class()._base_manager.all().order_by('id')[:5]
            hata.extend(model_hata)
        if with_respect_to:
            ct_model = ContentType.objects.filter(model=with_respect_to)
            if ct_model.exists():
                if with_respect_to in args:
                    return sorted(
                        hata, key=lambda x: x.__class__._meta.model_name.startswith(with_respect_to), reverse=True
                    )
        return hata


class LatestHata:
    objects = LatestHataManeger()


class CategoryManager(models.Manager):

    CATEGORY_NAME_COUNT_NAME = {
        'Новостройки': 'newbuilding__count',
        'Квартиры': 'flats__count',
        'Коттеджи': 'cottage__count'
    }

    def get_queryset(self):
        return super().get_queryset()

    def get_categories_for_sidebar(self):
        models = get_models_count('newbuilding', 'flats', 'cottage')
        qs = list(self.get_queryset().annotate(*models))
        data = [
            dict(name=c.name, url=c.get_absolute_url(), count=getattr(c, self.CATEGORY_NAME_COUNT_NAME[c.name]))
            for c in qs
        ]
        return data


class Category(models.Model):
    name = models.CharField(max_length=16, verbose_name='Категория_недвижимости')
    slug = models.SlugField(unique=True)
    objects = CategoryManager()

    def __str__(self):
        return f'{self.name}'

    class Meta:
        verbose_name = 'Категорию'
        verbose_name_plural = 'Категории'

    def get_absolute_url(self):
        return reverse('category_detail', kwargs={'slug': self.slug})


class RealEstate(models.Model):

    VALID_RES_MIN = (400, 400)
    VALID_RES_MAX = (900, 600)

    class Meta:
        abstract = True

    name = models.CharField(max_length=50, verbose_name='Наименование_недвижимости')
    about = models.CharField(max_length=50, verbose_name='Описание')
    square = models.CharField(max_length=50, verbose_name='Площадь')
    price = models.DecimalField(max_digits=20, decimal_places=2, default=Decimal('0.00'))
    slug = models.SlugField(unique=True)
    image = models.ImageField(verbose_name='Изображение')
    category = models.ForeignKey(Category, verbose_name='Категории', on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.name}'

    def get_model_name(self):
        return self.__class__.__name__.lower()


class NewBuilding(RealEstate):
    rooms = models.CharField(max_length=8, verbose_name='Количество комнат')
    floor = models.CharField(max_length=8, verbose_name='Этаж')

    def __str__(self):
        return f'{self.category.name}--{self.name}'

    class Meta:
        verbose_name = 'Новостройка'
        verbose_name_plural = 'Новостройки'

    def get_absolute_url(self):
        return get_real_estate_url(self, 'estate_detail')


class Flats(RealEstate):
    rooms = models.CharField(max_length=8, verbose_name='Количество комнат')
    floor = models.CharField(max_length=8, verbose_name='Этаж')

    def __str__(self):
        return f'{self.category.name}--{self.name}'

    class Meta:
        verbose_name = 'Квартира'
        verbose_name_plural = 'Квартиры'

    def get_absolute_url(self):
        return get_real_estate_url(self, 'estate_detail')


class Cottage(RealEstate):
    location = models.CharField(max_length=225, verbose_name='Местоположение')

    def __str__(self):
        return f'{self.category.name}--{self.name}'

    class Meta:
        verbose_name = 'Коттедж'
        verbose_name_plural = 'Коттеджи'

    def get_absolute_url(self):
        return get_real_estate_url(self, 'estate_detail')


class FavoriteRealEstate(models.Model):
    user = models.ForeignKey('Client', on_delete=models.CASCADE, blank=True, null=True, verbose_name='Покупатель')
    favorites = models.ForeignKey('Favorites', on_delete=models.CASCADE, related_name='related_real_estate',
                                 verbose_name='Избранное')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    qty = models.PositiveIntegerField(default=1)

    def __str__(self):
        return f'{self.content_object.name}'

    class Meta:
        verbose_name = 'Товары в избранное'
        verbose_name_plural = 'Товары в избранном'


class Favorites(models.Model):
    owner = models.ForeignKey('Client', on_delete=models.CASCADE, blank=True, null=True, verbose_name='Пользователь')
    real_estate = models.ManyToManyField(FavoriteRealEstate, blank=True, related_name='related_favorite')
    total_object = models.PositiveIntegerField(default=0)
    in_order = models.BooleanField(default=False)
    for_anonymous_user = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.id}'

    class Meta:
        verbose_name = 'Избранное'
        verbose_name_plural = 'Избранное'


class Client(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True, verbose_name='Пользователь')
    phone = models.CharField(max_length=20, blank=True, null=True, verbose_name='Номер телефона')
    adress = models.CharField(max_length=225,blank=True, null=True, verbose_name='Адрес')

    def __str__(self):
        return f'Пользователь: {self.user.first_name}--{self.user.last_name}'

    class Meta:
        verbose_name = 'Клиента'
        verbose_name_plural = 'Клиент'
