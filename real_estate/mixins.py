from django.views.generic.detail import SingleObjectMixin
from django.views.generic import View

from .models import Category, Client, NewBuilding, Flats, Cottage, Favorites


class CategoryDetailMixin(SingleObjectMixin):
    CATEGORY_MODEL_TO_REAL_ESTATE_MODEL = {
        'newbuilding': NewBuilding,
        'flats': Flats,
        'cottage': Cottage
    }
    def get_context_data(self, **kwargs):
        if isinstance(self.get_object(), Category):
            model = self.CATEGORY_MODEL_TO_REAL_ESTATE_MODEL[self.get_object().slug]
            context = super().get_context_data(**kwargs)
            context['categories'] = Category.objects.get_categories_for_sidebar()
            context['category_real_estate'] = model.objects.all()
            return context
        context = super().get_context_data(**kwargs)
        context['categories'] = Category.objects.get_categories_for_sidebar()
        return context


class FavoritesMixin(View):

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            client = Client.objects.filter(user=request.user).first()
            if not client:
                client = Client.objects.create(
                    user=request.user
                )
            favorites = Favorites.objects.filter(owner=client, in_order=False).first()
            if not favorites:
                favorites = Favorites.objects.create(owner=client)
        else:
            favorites = Favorites.objects.filter(for_anonymous_user=True).first()
            if not favorites:
                favorites = Favorites.objects.create(for_anonymous_user=True)
        self.favorites = favorites
        return super().dispatch(request, *args, **kwargs)
