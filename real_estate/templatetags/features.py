from django import template
from django.utils.safestring import mark_safe

register = template.Library()

TABLE_HEAD = """
                <table class="table">
                  <tbody>
             """

TABLE_TAIL = """
                  </tbody>
                </table>
             """

TABLE_CONTENT = """
                    <tr>
                      <td>{name}</td>
                      <td>{value}</td>
                    </tr>
                """

ESTATE_FEATURE = {
    'newbuilding': {
        'Количество комнат': 'rooms',
        'Этаж': 'floor'
    },
    'flats': {
        'Количество комнат': 'rooms',
        'Этаж': 'floor'
    },
    'cottage': {
        'Местоположение': 'location'
    }
}


def get_estate_feature(estate, model_name):
    table_content = ''
    for name, value in ESTATE_FEATURE[model_name].items():
        table_content += TABLE_CONTENT.format(name=name, value=getattr(estate, value))
    return table_content


@register.filter
def estate_feature(estate):
    model_name = estate.__class__._meta.model_name
    return mark_safe(TABLE_HEAD + get_estate_feature(estate, model_name) + TABLE_TAIL)
