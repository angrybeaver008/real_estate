from django.forms import ModelChoiceField, ModelForm, ValidationError
from django.contrib import admin
from .models import NewBuilding, Flats, Cottage, Category, FavoriteRealEstate, Favorites, Client, RealEstate
from PIL import Image


class NewBuildingAdminForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].help_text = f"Загрузите изображение размером не менее: " \
                                         f"{RealEstate.VALID_RES_MIN} и не более{RealEstate.VALID_RES_MAX} "

    def clean_image(self):
        image = self.cleaned_data['image']
        img = Image.open(image)
        min_height, min_width = RealEstate.VALID_RES_MIN
        max_height, max_width = RealEstate.VALID_RES_MAX
        if img.height < min_height or img.width < min_width:
            raise ValidationError('Изображение меньше минимального!')
        if img.height < max_height or img.width < max_width:
            raise ValidationError('Изображение больше максимального!')
        return image


class NewBuildingAdmin(admin.ModelAdmin):

    form = NewBuildingAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='newbuilding'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class FlatsAdmin(admin.ModelAdmin):

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='flats'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


class CottageAdmin(admin.ModelAdmin):

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='cottage'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Category)
admin.site.register(NewBuilding, NewBuildingAdmin)
admin.site.register(Flats, FlatsAdmin)
admin.site.register(Cottage, CottageAdmin)
admin.site.register(FavoriteRealEstate)
admin.site.register(Favorites)
admin.site.register(Client)
